/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.google.android.gms;

public final class R {
	public static final class attr {
		public static final int adSize = 0x7f01002c;
		public static final int adSizes = 0x7f01002d;
		public static final int adUnitId = 0x7f01002e;
		public static final int cameraBearing = 0x7f010030;
		public static final int cameraTargetLat = 0x7f010031;
		public static final int cameraTargetLng = 0x7f010032;
		public static final int cameraTilt = 0x7f010033;
		public static final int cameraZoom = 0x7f010034;
		public static final int mapType = 0x7f01002f;
		public static final int uiCompass = 0x7f010035;
		public static final int uiRotateGestures = 0x7f010036;
		public static final int uiScrollGestures = 0x7f010037;
		public static final int uiTiltGestures = 0x7f010038;
		public static final int uiZoomControls = 0x7f010039;
		public static final int uiZoomGestures = 0x7f01003a;
		public static final int useViewLifecycle = 0x7f01003b;
		public static final int zOrderOnTop = 0x7f01003c;
	}
	public static final class color {
		public static final int common_action_bar_splitter = 0x7f08001b;
		public static final int common_signin_btn_dark_text_default = 0x7f080012;
		public static final int common_signin_btn_dark_text_disabled = 0x7f080014;
		public static final int common_signin_btn_dark_text_focused = 0x7f080015;
		public static final int common_signin_btn_dark_text_pressed = 0x7f080013;
		public static final int common_signin_btn_default_background = 0x7f08001a;
		public static final int common_signin_btn_light_text_default = 0x7f080016;
		public static final int common_signin_btn_light_text_disabled = 0x7f080018;
		public static final int common_signin_btn_light_text_focused = 0x7f080019;
		public static final int common_signin_btn_light_text_pressed = 0x7f080017;
		public static final int common_signin_btn_text_dark = 0x7f08002f;
		public static final int common_signin_btn_text_light = 0x7f080030;
	}
	public static final class drawable {
		public static final int common_signin_btn_icon_dark = 0x7f02002d;
		public static final int common_signin_btn_icon_disabled_dark = 0x7f02002e;
		public static final int common_signin_btn_icon_disabled_focus_dark = 0x7f02002f;
		public static final int common_signin_btn_icon_disabled_focus_light = 0x7f020030;
		public static final int common_signin_btn_icon_disabled_light = 0x7f020031;
		public static final int common_signin_btn_icon_focus_dark = 0x7f020032;
		public static final int common_signin_btn_icon_focus_light = 0x7f020033;
		public static final int common_signin_btn_icon_light = 0x7f020034;
		public static final int common_signin_btn_icon_normal_dark = 0x7f020035;
		public static final int common_signin_btn_icon_normal_light = 0x7f020036;
		public static final int common_signin_btn_icon_pressed_dark = 0x7f020037;
		public static final int common_signin_btn_icon_pressed_light = 0x7f020038;
		public static final int common_signin_btn_text_dark = 0x7f020039;
		public static final int common_signin_btn_text_disabled_dark = 0x7f02003a;
		public static final int common_signin_btn_text_disabled_focus_dark = 0x7f02003b;
		public static final int common_signin_btn_text_disabled_focus_light = 0x7f02003c;
		public static final int common_signin_btn_text_disabled_light = 0x7f02003d;
		public static final int common_signin_btn_text_focus_dark = 0x7f02003e;
		public static final int common_signin_btn_text_focus_light = 0x7f02003f;
		public static final int common_signin_btn_text_light = 0x7f020040;
		public static final int common_signin_btn_text_normal_dark = 0x7f020041;
		public static final int common_signin_btn_text_normal_light = 0x7f020042;
		public static final int common_signin_btn_text_pressed_dark = 0x7f020043;
		public static final int common_signin_btn_text_pressed_light = 0x7f020044;
		public static final int ic_plusone_medium_off_client = 0x7f020059;
		public static final int ic_plusone_small_off_client = 0x7f02005a;
		public static final int ic_plusone_standard_off_client = 0x7f02005b;
		public static final int ic_plusone_tall_off_client = 0x7f02005c;
	}
	public static final class id {
		public static final int hybrid = 0x7f070016;
		public static final int none = 0x7f070013;
		public static final int normal = 0x7f070011;
		public static final int satellite = 0x7f070014;
		public static final int terrain = 0x7f070015;
	}
	public static final class integer {
		public static final int google_play_services_version = 0x7f0a0002;
	}
	public static final class string {
		public static final int auth_client_needs_enabling_title = 0x7f0b00a8;
		public static final int auth_client_needs_installation_title = 0x7f0b00a9;
		public static final int auth_client_needs_update_title = 0x7f0b00aa;
		public static final int auth_client_play_services_err_notification_msg = 0x7f0b00ab;
		public static final int auth_client_requested_by_msg = 0x7f0b00ac;
		public static final int auth_client_using_bad_version_title = 0x7f0b00a7;
		public static final int common_google_play_services_enable_button = 0x7f0b0099;
		public static final int common_google_play_services_enable_text = 0x7f0b0098;
		public static final int common_google_play_services_enable_title = 0x7f0b0097;
		public static final int common_google_play_services_install_button = 0x7f0b0096;
		public static final int common_google_play_services_install_text_phone = 0x7f0b0094;
		public static final int common_google_play_services_install_text_tablet = 0x7f0b0095;
		public static final int common_google_play_services_install_title = 0x7f0b0093;
		public static final int common_google_play_services_invalid_account_text = 0x7f0b009f;
		public static final int common_google_play_services_invalid_account_title = 0x7f0b009e;
		public static final int common_google_play_services_network_error_text = 0x7f0b009d;
		public static final int common_google_play_services_network_error_title = 0x7f0b009c;
		public static final int common_google_play_services_unknown_issue = 0x7f0b00a0;
		public static final int common_google_play_services_unsupported_date_text = 0x7f0b00a3;
		public static final int common_google_play_services_unsupported_text = 0x7f0b00a2;
		public static final int common_google_play_services_unsupported_title = 0x7f0b00a1;
		public static final int common_google_play_services_update_button = 0x7f0b00a4;
		public static final int common_google_play_services_update_text = 0x7f0b009b;
		public static final int common_google_play_services_update_title = 0x7f0b009a;
		public static final int common_signin_button_text = 0x7f0b00a5;
		public static final int common_signin_button_text_long = 0x7f0b00a6;
	}
	public static final class styleable {
		public static final int[] AdsAttrs = { 0x7f01002c, 0x7f01002d, 0x7f01002e };
		public static final int AdsAttrs_adSize = 0;
		public static final int AdsAttrs_adSizes = 1;
		public static final int AdsAttrs_adUnitId = 2;
		public static final int[] MapAttrs = { 0x7f01002f, 0x7f010030, 0x7f010031, 0x7f010032, 0x7f010033, 0x7f010034, 0x7f010035, 0x7f010036, 0x7f010037, 0x7f010038, 0x7f010039, 0x7f01003a, 0x7f01003b, 0x7f01003c };
		public static final int MapAttrs_cameraBearing = 1;
		public static final int MapAttrs_cameraTargetLat = 2;
		public static final int MapAttrs_cameraTargetLng = 3;
		public static final int MapAttrs_cameraTilt = 4;
		public static final int MapAttrs_cameraZoom = 5;
		public static final int MapAttrs_mapType = 0;
		public static final int MapAttrs_uiCompass = 6;
		public static final int MapAttrs_uiRotateGestures = 7;
		public static final int MapAttrs_uiScrollGestures = 8;
		public static final int MapAttrs_uiTiltGestures = 9;
		public static final int MapAttrs_uiZoomControls = 10;
		public static final int MapAttrs_uiZoomGestures = 11;
		public static final int MapAttrs_useViewLifecycle = 12;
		public static final int MapAttrs_zOrderOnTop = 13;
	}
}
