package com.gamaprime.definecebinde;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.graphics.Bitmap;
import android.os.Environment;

public class Unzip extends PostExecuteStrategies{
	
	@Override
	public void make() {
		// TODO Auto-generated method stub
		File _file = new File(path,directory);
		if (!_file.exists())
			_file.mkdirs();
		try {
			Bitmap mBackground = null;
			FileInputStream fis = new FileInputStream(
					path +"/"+file);
			ZipInputStream zis = new ZipInputStream(fis);
			ZipEntry ze = null;
			while ((ze = zis.getNextEntry()) != null) {
				FileOutputStream fout = new FileOutputStream(path + "/"+directory+"/"+ ze.getName());
				byte[] buffer = new byte[8192];
				int len;
				while ((len = zis.read(buffer)) != -1) {
					fout.write(buffer, 0, len);
				}
				fout.close();

				zis.closeEntry();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
