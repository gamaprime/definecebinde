package com.gamaprime.definecebinde;

public abstract class PostExecuteStrategies {
	protected String path;
	protected String directory;
	protected String file;
	public void setPath(String _path)
	{
		path=_path;
	}
	public void setDirectory(String _directory)
	{
		directory=_directory;
	}
	public void setFile(String _file)
	{
		file=_file;
	}
	public String getPath()
	{
		return path;
	}
	public String getDirectory()
	{
		return directory;
	}
	public String getFile()
	{
		return file;
	}
	public abstract void make();
}
