package com.gamaprime.definecebinde;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.os.AsyncTask;


public class FileDownloader extends AsyncTask<String, String, Boolean> {
	private String path;
	private String directory;
	private String file;
	private PostExecuteStrategies strategy;
	public static Context context;
	public void setStrategy(PostExecuteStrategies _strategy)
	{
		strategy=_strategy;
		strategy.setDirectory(directory);
		strategy.setFile(file);
		strategy.setPath(path);
	}
	public void setPath(String _path)
	{
		path=_path;
	}
	public void setDirectory(String _directory)
	{
		directory=_directory;
	}
	public void setFile(String _file)
	{
		file=_file;
	}
	public String getPath()
	{
		return path;
	}
	public String getDirectory()
	{
		return directory;
	}
	public String getFile()
	{
		return file;
	}
	@Override
	protected Boolean doInBackground(String... urls) {
		// TODO Auto-generated method stub
		int count;
		
		try {

		URL url = new URL(urls[0]);
		URLConnection conexion = url.openConnection();
		conexion.connect();

		//lenghtOfFile = conexion.getContentLength();
		

		InputStream input = new BufferedInputStream(url.openStream());
		FileOutputStream output=context.openFileOutput(file, Context.MODE_PRIVATE);
		
		byte data[] = new byte[1024*32];

		long total = 0;

			while ((count = input.read(data)) != -1) {
				total += count;
				//publishProgress(""+(int)((total*100)/lenghtOfFile));
				output.write(data, 0, count);
				if(isCancelled())
					return false;
			}

			output.flush();
			output.close();
			input.close();
		} catch (Exception e) {}
		return true;
		
	}
	protected void onPostExecute(Boolean result) {
		if(result && strategy!=null)
		{
			strategy.make();
		}
			
	}

}
