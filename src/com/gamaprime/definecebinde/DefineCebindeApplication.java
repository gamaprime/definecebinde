package com.gamaprime.definecebinde;



import android.app.Application;
import android.preference.PreferenceManager;

import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseTwitterUtils;
import com.shephertz.app42.paas.sdk.android.App42API;
import com.shephertz.app42.paas.sdk.android.App42CallBack;
import com.shephertz.app42.paas.sdk.android.user.User;
import com.shephertz.app42.paas.sdk.android.user.UserService;


public class DefineCebindeApplication extends Application {
	public static String fullName="";
	
	@Override
	public void onCreate() {
		  super.onCreate();
		  
		  App42API.initialize(this,"5ee6cf363150c9b2f8b2b41012db267d90b53d91b29b10ff103cec4b10065400","c2e2ef86522fed1bceab77aff829de711be7965251a8a4cb87f21078c652a3d7");  
		  Parse.initialize(this, "CLSmNYcKJzDVqs3KxhGriTBeP0xQzGwNd1aAaSKh", "8Kq2qWHlh2QviCAgJv6N2RaSdT4r5eUxFGMT9c03");
		  ParseFacebookUtils.initialize("585938988165750");
		  ParseTwitterUtils.initialize("0w5ZZEGrODhm7b1AhyVpqg", "6eWJaRywLUZ5SITOS39ajPxtnVEu65xFgVQ0Vfh4o");
		  
		  String userName=PreferenceManager.getDefaultSharedPreferences(getBaseContext()).getString("username", "");
		  if(!userName.equals(""))
		  {
			  UserService userService=App42API.buildUserService();
			  userService.getUser(userName, new App42CallBack() {  
				  public void onSuccess(Object response)   
				  {  
				      User user = (User)response;
				      fullName=user.getProfile().getFirstName()+" "+user.getProfile().getLastName();         
				  }  
				  public void onException(Exception ex)   
				  {  
				        
				  }  
			  });     
		  }
		  
	}
	
}
