package com.gamaprime.definecebinde;



import org.json.JSONException;
import org.json.JSONObject;

import com.shephertz.app42.paas.sdk.android.App42API;
import com.shephertz.app42.paas.sdk.android.App42CallBack;
import com.shephertz.app42.paas.sdk.android.customcode.CustomCodeService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;

public class LoadingActivity extends Activity {
	private static final long DURATION=1500;
	private String username="";
	private String documentId="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loading);
		username=getIntent().getStringExtra("username");
		documentId=getIntent().getStringExtra("gameDocumentId");
		final ImageView view=(ImageView)findViewById(R.id.siyah);
		final ImageView viewRenkli=(ImageView)findViewById(R.id.renkli);
		final Animation fadeIn = AnimationUtils.loadAnimation(this, R.anim.anim_fade_in);
		final Animation fadeOut = AnimationUtils.loadAnimation(this, R.anim.anim_fade_out);
		fadeIn.setDuration(DURATION);
		fadeOut.setDuration(DURATION);
		fadeIn.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				view.startAnimation(fadeOut);
			}
		});
		fadeOut.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				view.startAnimation(fadeIn);
			}
		});
		view.startAnimation(fadeOut);
		if(getIntent().getIntExtra("status", -1)==0)
			startTheGame();
		else if(getIntent().getIntExtra("status", -1)==1)
			startMissionActivity();
		else if(getIntent().getIntExtra("status", -1)==2)
			continueTheGame();
	}
	public void startTheGame()
	{
		DatabaseHelper db=new DatabaseHelper(this);
		db.updatePlayedGames(documentId);
		JSONObject userInfo=new JSONObject();
		try {
			userInfo.put("username", username);
			userInfo.put("gameDocumentId", documentId);
			CustomCodeService customCode=App42API.buildCustomCodeService();
			customCode.runJavaCode("CreateUserHistory", userInfo, new App42CallBack() {
				
				@Override
				public void onSuccess(Object arg0) {
					// TODO Auto-generated method stub
					JSONObject jsonObject=(JSONObject)arg0;
					try {
						int questionType=jsonObject.getInt("questionType");
						String questionInfo=jsonObject.getString("questionInfo");
						startQuestionActivity(questionType, questionInfo, "",documentId);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				@Override
				public void onException(Exception arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void startQuestionActivity(int questionType,String questionInfo,String imageUrl,String gameDocumentId)
	{
		switch(questionType){
			case 0:
			{
				Intent i=new Intent(LoadingActivity.this,OnlyQuestion_Activity.class);
				i.putExtra("questionInfo", questionInfo);
				i.putExtra("gameDocumentId", gameDocumentId);
				i.putExtra("username", username);
				startActivity(i);
				finish();
				break;
			}
			case 1:
			{
				break;
			}
			case 2:
			{
				break;
			}
			default:
				break;
		}
	}
	public void startMissionActivity()
	{
		
		String questionInfo=getIntent().getStringExtra("questionInfo");
		int missionType=getIntent().getIntExtra("missionType", -1);
		int questionType=getIntent().getIntExtra("questionType", -1);
		String imageUrl=getIntent().getStringExtra("imageUrl");
		switch(questionType){
		case 0:
		{
			Intent i=new Intent(LoadingActivity.this,Mission_Text_Only.class);
			i.putExtra("questionInfo", questionInfo);
			i.putExtra("gameDocumentId", documentId);
			i.putExtra("username", username);
			i.putExtra("missionType", missionType);
			startActivity(i);
			finish();
			break;
		}
		case 1:
		{
			break;
		}
		case 2:
		{
			break;
		}
		default:
			break;
	}
	}
	public void continueTheGame()
	{
		JSONObject userInfo=new JSONObject();
		try {
			userInfo.put("username", username);
			userInfo.put("gameDocumentId", documentId);
			CustomCodeService customCode=App42API.buildCustomCodeService();
			customCode.runJavaCode("PlayedGamesSystem", userInfo, new App42CallBack() {
				
				@Override
				public void onSuccess(Object arg0) {
					// TODO Auto-generated method stub
					JSONObject jsonObject=(JSONObject)arg0;
					
					try {
						if(jsonObject.getString("gameStatus").equals("Q"))
						{
							int questionType=jsonObject.getInt("questionType");
							String questionInfo=jsonObject.getString("questionInfo");
							startQuestionActivity(questionType, questionInfo, "",documentId);
						}
						else
						{
							
						}
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				@Override
				public void onException(Exception arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
