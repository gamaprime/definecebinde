package com.gamaprime.definecebinde;


import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		
		
		if(PreferenceManager.getDefaultSharedPreferences(getBaseContext()).getBoolean("IsLoggedIn", false)) 
		{ 
			Intent intent=new Intent(MainActivity.this,Activity_Navigation.class); 
			startActivity(intent);
			finish(); 
		}
		
		 
		
		
		Button signInButton = (Button) findViewById(R.id.activity_start_signInButton);
		Button signUpButton = (Button) findViewById(R.id.activity_start_signUpButton);
		signInButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				signIn();
			}
		});
		signUpButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				signUp();
			}
		});
		
		
	}

	public void signIn() {
		Intent intent = new Intent(MainActivity.this, SignInActivity.class);
		startActivity(intent);
	}

	public void signUp() {
		Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onStop() {
		super.onStop();
		finish();
	}

}
