package com.gamaprime.definecebinde;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
public class MapActivity extends Activity {
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);
        ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
        GoogleMap map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        int position=getIntent().getIntExtra("position", -1);
        
        if(position!=-1)
        {
        	LatLng gameLocation=new LatLng(Activity_Navigation.LATITUDES.get(position), Activity_Navigation.LONGITUDES.get(position));
        	map.setMyLocationEnabled(true);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(gameLocation, 13));
            map.addMarker(new MarkerOptions()
            .title(Activity_Navigation.GAMENAMES.get(position))
            .snippet(Activity_Navigation.GAMEDESCRIPTIONS.get(position))
            .position(gameLocation));
        }
    }
}
