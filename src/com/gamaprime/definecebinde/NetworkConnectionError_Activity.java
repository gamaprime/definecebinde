package com.gamaprime.definecebinde;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
public class NetworkConnectionError_Activity extends Activity {
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		/*findViewById(R.id.tryagain).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isConnected())
				{
					Intent i=new Intent(NetworkConnectionError_Activity.this,Activity_Navigation.class);
					startActivity(i);
					finish();
				}
				else
				{
					Toast.makeText(getApplicationContext(),R.string.still_no_connection_error, Toast.LENGTH_SHORT).show();
				}
				
			}
		});*/
	}
	public boolean isConnected()
	{
		ConnectivityManager cm =
		        (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean _isConnected = activeNetwork != null &&
		                      activeNetwork.isConnectedOrConnecting();
		return _isConnected;
	}
}
