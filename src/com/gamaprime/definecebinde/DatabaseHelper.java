package com.gamaprime.definecebinde;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.DropBoxManager;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper{
	private static final int DATABASE_VERSION=1;
	private static final String DATABASE_NAME="GAMESDATABASE";
	private static final String TABLE_GAMES="games";
	private static final String KEY_ID="id";
	private static final String KEY_DOCUMENTID="documentId";
	private static final String KEY_GAMENAME="gameName";
	private static final String KEY_LOCATIONNAME="locationName";
	private static final String KEY_GAMEDESCRIPTION="gameDescription";
	private static final String KEY_ZIPPATH="zipPath";
	private static final String KEY_GAMELATITUDE="gameLatitude";
	private static final String KEY_GAMELONGITUDE="gameLatitude";
	private static final String KEY_PLAYEDBEFORE="playedBefore";
	private static final String KEY_LASTDATE="lastDate";
	private static final String KEY_ICONURL="iconUrl";
	private static final String KEY_SCORE="score";
	private static final String KEY_TOTALPOINTS="totalPoints";
	private static final String KEY_PASTPOINTS="pastPoints";
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
		String CREATE_GAMES_TABLE="CREATE TABLE games ( "+"id INTEGER PRIMARY KEY AUTOINCREMENT, "+"documentId TEXT, "
		+"gameName TEXT, "+"locationName TEXT, "+"gameDescription TEXT, "+"zipPath TEXT, "+"gameLatitude DOUBLE, "
				+"gameLongitude DOUBLE, "+"playedBefore INT, "+"lastDate TEXT, "+"iconUrl TEXT)";
		db.execSQL(CREATE_GAMES_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS games");
		this.onCreate(db);
	}
	public void dropTable()
	{
		Log.d("AAA","EEE");
		SQLiteDatabase db=getWritableDatabase();
		db.execSQL("DROP TABLE IF EXISTS games");
		Log.d("BBB","DDDD");
	}
	public void createTable()
	{
		SQLiteDatabase db=getWritableDatabase();
		String CREATE_GAMES_TABLE="CREATE TABLE IF NOT EXISTS games ( "+"id INTEGER PRIMARY KEY AUTOINCREMENT, "+"documentId TEXT, "
				+"gameName TEXT, "+"locationName TEXT, "+"gameDescription TEXT, "+"zipPath TEXT, "+"gameLatitude DOUBLE, "
						+"gameLongitude DOUBLE, "+"playedBefore INT, "+"lastDate TEXT, "+"iconUrl TEXT, "+"score INT, "
						+"totalPoints INT, "+"pastPoints INT)";
				db.execSQL(CREATE_GAMES_TABLE);
	}
	public void addGame(Game game)
	{
		SQLiteDatabase db=getWritableDatabase();
		ContentValues values=new ContentValues();
		values.put(KEY_DOCUMENTID, game.getDocumentId());
		values.put(KEY_GAMENAME, game.getGameName());
		values.put(KEY_LOCATIONNAME, game.getLocationName());
		values.put(KEY_GAMEDESCRIPTION, game.getGameDescription());
		values.put(KEY_ZIPPATH, game.getZipPath());
		values.put(KEY_GAMELATITUDE, game.getGameLatitude());
		values.put(KEY_GAMELONGITUDE, game.getGameLongitude());
		values.put(KEY_PLAYEDBEFORE, game.isPlayedBefore());
		values.put(KEY_LASTDATE, game.getLastDate());
		values.put(KEY_ICONURL, game.getIconUrl());
		values.put(KEY_SCORE, game.getScore());
		values.put(KEY_TOTALPOINTS, game.getTotalPath());
		values.put(KEY_PASTPOINTS, game.getPastPathCount());
		db.insert(TABLE_GAMES, null, values);
		db.close();
	}
	public List<Game> getAllGames()
	{
		List<Game> gameList=new ArrayList<Game>();
		String query="SELECT * FROM "+TABLE_GAMES;
		SQLiteDatabase db=getWritableDatabase();
		Cursor cursor=db.rawQuery(query, null);
		Game game=null;
		if(cursor.moveToFirst())
		{
			do{
				game=new Game();
				game.setDocumentId(cursor.getString(1));
				game.setGameName(cursor.getString(2));
				game.setLocationName(cursor.getString(3));
				game.setGameDescription(cursor.getString(4));
				game.setZipPath(cursor.getString(5));
				game.setGameLatitude(cursor.getDouble(6));
				game.setGameLongitude(cursor.getDouble(7));
				game.setPlayedBefore(cursor.getInt(8));
				game.setLastDate(cursor.getString(9));
				game.setIconUrl(cursor.getString(10));
				game.setScore(cursor.getInt(11));
				game.setTotalPath(cursor.getInt(12));
				game.setPastPathCount(cursor.getInt(13));
				gameList.add(game);
			}while(cursor.moveToNext());
			
		}
		return gameList;
	}
	public void updatePlayedGames(String documentId)
	{
		String query="UPDATE games SET playedBefore=1 WHERE documentId='"+documentId+"'";
		SQLiteDatabase db=getWritableDatabase();
		db.execSQL(query);
	}
	public void updateGame(Game game)
	{
		SQLiteDatabase db=getWritableDatabase();
		ContentValues values=new ContentValues();
		values.put(KEY_DOCUMENTID, game.getDocumentId());
		values.put(KEY_GAMENAME, game.getGameName());
		values.put(KEY_LOCATIONNAME, game.getLocationName());
		values.put(KEY_GAMEDESCRIPTION, game.getGameDescription());
		values.put(KEY_ZIPPATH, game.getZipPath());
		values.put(KEY_GAMELATITUDE, game.getGameLatitude());
		values.put(KEY_GAMELONGITUDE, game.getGameLongitude());
		values.put(KEY_PLAYEDBEFORE, game.isPlayedBefore());
		values.put(KEY_LASTDATE, game.getLastDate());
		values.put(KEY_ICONURL, game.getIconUrl());
		values.put(KEY_SCORE, game.getScore());
		values.put(KEY_TOTALPOINTS, game.getTotalPath());
		values.put(KEY_PASTPOINTS, game.getPastPathCount());
		db.update(TABLE_GAMES, values, KEY_DOCUMENTID+" = ?",new String[]{game.getDocumentId()} );
		db.close();
	}
	
}
