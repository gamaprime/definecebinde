package com.gamaprime.definecebinde;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import android.location.Address;
import android.location.Location;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.parse.ParseFacebookUtils;

import com.shephertz.app42.paas.sdk.android.App42API;
import com.shephertz.app42.paas.sdk.android.App42CallBack;
import com.shephertz.app42.paas.sdk.android.social.Social;
import com.shephertz.app42.paas.sdk.android.social.SocialService;
import java.lang.Math;

import org.apache.http.impl.conn.SingleClientConnManager;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
public class MenuActivity extends Activity {
	public static final int HDR_POS1 = 0;
    public static final int HDR_POS2 = 5;
    public static String[] HEADERS={"XX",null};
    public static final String[] DAY = { null,"1"};
    public static final String[] MONTH = { null,"June"};
    private static final Integer LIST_HEADER = 0;
    private static final Integer LIST_ITEM = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lv = (ListView)findViewById(R.id.activity_main_mainList);
        lv.setAdapter(new MyListAdapter(this));
    }

    private class MyListAdapter extends BaseAdapter {
        public MyListAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return DAY.length;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            String headerText = HEADERS[position];
            if(headerText != null) {

                View item = convertView;
                if(convertView == null || convertView.getTag() == LIST_ITEM) {

                    item = LayoutInflater.from(mContext).inflate(
                            R.layout.list_header_main_screen, parent, false);
                    item.setTag(LIST_HEADER);

                }

                TextView headerTextView = (TextView)item.findViewById(R.id.list_header_main_screen_listHeader);
                headerTextView.setText(headerText);
                return item;
            }

            View item = convertView;
            if(convertView == null || convertView.getTag() == LIST_HEADER) {
                item = LayoutInflater.from(mContext).inflate(
                        R.layout.list_row_games_not_attended, parent, false);
                item.setTag(LIST_ITEM);
                
            }

            TextView day = (TextView)item.findViewById(R.id.list_row_games_not_attended_calendarDay);
            day.setText(DAY[position]);

            TextView month = (TextView)item.findViewById(R.id.list_row_games_not_attended_calendarMonth);
            month.setText(MONTH[position]);
            
            
            
            //Set last divider in a sublist invisible
            View divider = item.findViewById(R.id.list_header_main_screen_itemSeparator);
            


            return item;
        }

        

        private final Context mContext;

		
    }

	
	
	
	
	
	/*private boolean pendingPublishReauthorization = false;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViewById(R.id.share).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Session session = ParseFacebookUtils.getSession();
				
			    if (session != null && session.isOpened()) {
			    	
			    	if (!isSubsetOf(Arrays.asList("publish_actions"), session.getPermissions())) {
			            pendingPublishReauthorization = true;
			            Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(MenuActivity.this, Arrays.asList("publish_actions"));
			            session.requestNewPublishPermissions(newPermissionsRequest);
						
			        }
			    	Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),new Request.GraphUserCallback() {
						
						@Override
						public void onCompleted(GraphUser user, Response response) {
							// TODO Auto-generated method stub
							SocialService socialService=App42API.buildSocialService();
							socialService.updateFacebookStatus(user.getId(), "Hi, I'm using Define Cebinde Application", new App42CallBack() {  
								public void onSuccess(Object response)   
								{  
								    Social social  = (Social)response;  
								    System.out.println("userName is " + social.getUserName());  
								    System.out.println("status is " + social.getStatus());  
								    String jsonResponse = social.toString();     
								}  
								public void onException(Exception ex)   
								{  
								    System.out.println("Exception Message"+ex.getMessage());  
								}  
							});
						}
						
					});
			    	request.executeAsync();
			    }
				
			}
		});
	}
	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
        for (String string : subset) {
            if (!superset.contains(string)) {
                return false;
            }
        }
        return true;
    }*/
    
}
