/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gamaprime.definecebinde;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;

import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;


import com.facebook.Session;

import com.parse.ParseFacebookUtils;
import com.shephertz.app42.paas.sdk.android.App42API;
import com.shephertz.app42.paas.sdk.android.App42CallBack;

import com.shephertz.app42.paas.sdk.android.avatar.AvatarService;
import com.shephertz.app42.paas.sdk.android.customcode.CustomCodeService;

import com.shephertz.app42.paas.sdk.android.storage.Query;
import com.shephertz.app42.paas.sdk.android.storage.QueryBuilder;
import com.shephertz.app42.paas.sdk.android.storage.Storage;
import com.shephertz.app42.paas.sdk.android.storage.StorageService;
import com.shephertz.app42.paas.sdk.android.storage.QueryBuilder.Operator;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import android.os.AsyncTask;
import android.os.Bundle;

import android.preference.PreferenceManager;

import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This example illustrates a common usage of the DrawerLayout widget in the
 * Android support library.
 * <p/>
 * <p>
 * When a navigation (left) drawer is present, the host activity should detect
 * presses of the action bar's Up affordance as a signal to open and close the
 * navigation drawer. The ActionBarDrawerToggle facilitates this behavior. Items
 * within the drawer should fall into one of two categories:
 * </p>
 * <p/>
 * <ul>
 * <li><strong>View switches</strong>. A view switch follows the same basic
 * policies as list or tab navigation in that a view switch does not create
 * navigation history. This pattern should only be used at the root activity of
 * a task, leaving some form of Up navigation active for activities further down
 * the navigation hierarchy.</li>
 * <li><strong>Selective Up</strong>. The drawer allows the user to choose an
 * alternate parent for Up navigation. This allows a user to jump across an
 * app's navigation hierarchy at will. The application should treat this as it
 * treats Up navigation from a different task, replacing the current task stack
 * using TaskStackBuilder or similar. This is the only form of navigation drawer
 * that should be used outside of the root activity of a task.</li>
 * </ul>
 * <p/>
 * <p>
 * Right side drawers should be used for actions, not navigation. This follows
 * the pattern established by the Action Bar that navigation should be to the
 * left and actions to the right. An action should be an operation performed on
 * the current contents of the window, for example enabling or disabling a data
 * overlay on top of the current content.
 * </p>
 */
public class Activity_Navigation extends Activity {
	

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private static int RESULT_LOAD_IMAGE = 1;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private String[] mTitles;
	public static final int HDR_POS1 = 0;
	public static final int HDR_POS2 = 5;
	public static ArrayList<String> HEADERS;
	public static ArrayList<String> DAY;
	public static ArrayList<String> MONTH;
	public static ArrayList<String> GAMENAMES;
	public static ArrayList<String> GAMELOCATIONS;
	public static ArrayList<String> GAMEDESCRIPTIONS;
	public static ArrayList<Double> LATITUDES;
	public static ArrayList<Double> LONGITUDES;
	public static ArrayList<String> DOCUMENTIDS;
	public static ArrayList<Integer> PLAYEDBEFORE;
	public static ArrayList<Integer> SCORES;
	public static ArrayList<Integer> TOTALPOINTS;
	public static ArrayList<Integer> PASTPOINTS;
	private static final Integer LIST_HEADER = 0;
	private static final Integer LIST_ITEM = 1;
	public static Context context;
	public static String _username;
	public static String avatarPath;
	private static String DBNAME = "GAMESDB";
	private static String COLLECTIONNAME = "GamesCollection";
	public static ArrayList<String> ICONURLS;
	private static String ICONEXTENSION = "_icon.jpg";
	
	public static boolean isLoading;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (!isConnected()) {
			Intent i = new Intent(this, NetworkConnectionError_Activity.class);
			startActivity(i);
			finish();
		}

		setContentView(R.layout.activity_navigation_drawer);

		isLoading = true;
		context = getBaseContext();
		_username = PreferenceManager.getDefaultSharedPreferences(context)
				.getString("username", "");
		avatarPath = getFilesDir() + "/" + _username + ".jpg";
		initializeLists();
		setHeader(DAY.size(), getString(R.string.header_current_games));
		getGameInformation();
		mTitle = mDrawerTitle = getTitle();
		mTitles = getResources().getStringArray(R.array.drawer_array);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		
		// set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		// set up the drawer's list view with items and click listener
		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_list_item, mTitles));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description for accessibility */
		R.string.drawer_close /* "close drawer" description for accessibility */
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			selectItem(0);
		}
		
		
	}

	public void showErrorMessage(final int message) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle(R.string.error);
				builder.setMessage(message);
				builder.setNegativeButton(R.string.ok_button,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.cancel();
							}
						});
				AlertDialog alertDialog = builder.create();
				alertDialog.show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content
		// view
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action buttons
		switch (item.getItemId()) {
		case R.id.action_settings:
			// create intent to perform web search for this planet
			Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
			intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
			// catch event that there's no activity to handle intent
			if (intent.resolveActivity(getPackageManager()) != null) {
				startActivity(intent);
			} else {
				Toast.makeText(this, R.string.app_not_available,
						Toast.LENGTH_LONG).show();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	private void selectItem(int position) {
		Fragment fragment = null;
		FragmentManager fragmentManager = getFragmentManager();
		switch (position) {
		case 0:
			fragment = new MainMenu();

			fragmentManager.beginTransaction()
					.replace(R.id.content_frame, fragment).commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			setTitle(mTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		case 1:
			fragment = new ProfileInfo();
			fragmentManager.beginTransaction()
					.replace(R.id.content_frame, fragment).commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			setTitle(mTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		case 2:
			fragment = new ProfileInfo();
			fragmentManager.beginTransaction()
					.replace(R.id.content_frame, fragment).commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			setTitle(mTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
			break;
		case 3:
			logout();
			break;
		default:
			break;
		}

	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/**
	 * Fragment that appears in the "content_frame", shows a planet
	 */

	public static class ProfileInfo extends Fragment {
		public static ImageView avatar;
		public static Uri selectedImage;
		public int count;

		public ProfileInfo() {

		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_profile_info,
					container, false);
			TextView _userName = (TextView) rootView
					.findViewById(R.id.fragment_profile_info_username);
			_userName.setText(_username);
			TextView _fullname = (TextView) rootView
					.findViewById(R.id.fragment_profile_info_fullname);
			_fullname.setText(DefineCebindeApplication.fullName);
			avatar = (ImageView) rootView
					.findViewById(R.id.fragment_profile_info_avatar);
			avatar.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					i.setType("image/*");
					startActivityForResult(i, RESULT_LOAD_IMAGE);
				}
			});
			Bitmap bmp = BitmapFactory.decodeFile(avatarPath);
			avatar.setImageBitmap(bmp);
			return rootView;
		}

		@Override
		public void onActivityResult(int requestCode, int resultCode,
				Intent data) {
			super.onActivityResult(requestCode, resultCode, data);

			if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
					&& null != data) {
				selectedImage = data.getData();
				FileOutputStream output = null;
				try {
					InputStream is = context.getContentResolver()
							.openInputStream(selectedImage);
					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inSampleSize = 8;
					Bitmap bmp = BitmapFactory.decodeStream(is, null, options);

					if (bmp.getHeight() < 2048 && bmp.getWidth() < 2048) {
						avatar.setImageBitmap(bmp);

						output = context.openFileOutput(_username + ".jpg",
								Context.MODE_PRIVATE);// new
														// FileOutputStream(avatarPath);
						bmp.compress(CompressFormat.JPEG, 20, output);
						String timeStamp = new SimpleDateFormat(
								"yyyyMMdd_HHmmss", Locale.getDefault())
								.format(new Date());
						AvatarService avatarService = App42API
								.buildAvatarService();
						
						avatarService.createAvatar(timeStamp, _username,
								avatarPath, "Avatar of " + _username,
								new App42CallBack() {

									@Override
									public void onSuccess(Object arg0) {
										// TODO Auto-generated method stub

									}

									@Override
									public void onException(Exception arg0) {
										// TODO Auto-generated method stub

									}
								});
						avatarService.changeCurrentAvatar(_username, timeStamp,
								new App42CallBack() {

									@Override
									public void onSuccess(Object arg0) {
										// TODO Auto-generated method stub

									}

									@Override
									public void onException(Exception arg0) {
										// TODO Auto-generated method stub

									}
								});
					}

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					try {
						output.close();
					} catch (Throwable ignore) {
					}
				}
			}
		}

	}

	public static class MainMenu extends Fragment implements OnRefreshListener {
		private PullToRefreshLayout mPullToRefreshLayout;
		
		public MyListAdapter myAdapter;
		public MainMenu() {

		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.activity_pulltorefresh, container,
					false);
			ListView lv = (ListView) rootView
					.findViewById(R.id.activity_pull_mainList);
			myAdapter=new MyListAdapter(Activity_Navigation.context);
			lv.setAdapter(myAdapter);
			/*
			 * ListView lve = (ListView) rootView
			 * .findViewById(R.id.activity_main_mainListe); lve.setAdapter(new
			 * MyListAdapter(Activity_Navigation.context));
			 */
			if (!isLoading) {
				rootView.findViewById(R.id.activity_pull_loadingFrame)
						.setVisibility(View.GONE);
			}
			mPullToRefreshLayout = (PullToRefreshLayout) rootView.findViewById(R.id.ptr_layout);
			
			
		    // Now setup the PullToRefreshLayout
		    ActionBarPullToRefresh.from(getActivity())
		            // Mark All Children as pullable
		            .allChildrenArePullable()
		            // Set a OnRefreshListener
		            .listener(this)
		            // Finally commit the setup to our PullToRefreshLayout
		            .setup(mPullToRefreshLayout);
			return rootView;
		}

		@Override
		public void onRefreshStarted(View view) {
			// TODO REFRESH THE LISTS
			
			new AsyncTask<Void, Void, Void>() {

				@Override
				protected Void doInBackground(Void... params) {
					// TODO Auto-generated method stub
					refreshTheDatabase();
					DatabaseHelper db=new DatabaseHelper(context);
					List<Game> gamesList=db.getAllGames();
					for(int i=0;i<gamesList.size();i++)
					{
						Game game=gamesList.get(i);
						LATITUDES.add(game.getGameLatitude());
						LONGITUDES.add(game.getGameLongitude());
						GAMENAMES.add(game.getGameName());
						GAMEDESCRIPTIONS.add(game.getGameDescription());
						GAMELOCATIONS.add(game.getLocationName());
						DOCUMENTIDS.add(game.getDocumentId());
						ICONURLS.add(game.getIconUrl());
						PLAYEDBEFORE.add(game.isPlayedBefore());
						SCORES.add(game.getScore());
						TOTALPOINTS.add(game.getTotalPath());
						PASTPOINTS.add(game.getPastPathCount());
						
						String lastDate=game.getLastDate();
						DateFormat jsonFormat = new SimpleDateFormat(
								"yyyy-MM-dd'T'HH:mm:ss", Locale
										.getDefault());
						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MMMM-dd'T'HH:mm:ss", Locale
										.getDefault());
						Date date;
						try {
							date = jsonFormat.parse(lastDate);
							String format = dateFormat.format(date);
							String month = format.split("-")[1];
							String day = lastDate.split("-")[2].split("T")[0];
							DAY.add(day);
							MONTH.add(month);
							
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					try {
						Thread.sleep(1500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return null;
				}
				@Override
	            protected void onPostExecute(Void result) {
					
					Fragment fragment = null;
					FragmentManager fragmentManager = getFragmentManager();
					
					fragment = new MainMenu();
					fragmentManager.beginTransaction()
							.replace(R.id.content_frame, fragment).commit();
					mPullToRefreshLayout.setRefreshComplete();
				}
				
			}.execute();

		}
	}

	public void logout() {
		Session session = ParseFacebookUtils.getSession();

		if (session != null && session.isOpened()) {
			session.closeAndClearTokenInformation();

		}
		Editor edit = PreferenceManager.getDefaultSharedPreferences(
				getBaseContext()).edit();
		edit.putBoolean("IsLoggedIn", false);
		edit.commit();
		Intent intent = new Intent(Activity_Navigation.this, MainActivity.class);

		startActivity(intent);
		finish();
	}

	public static class MyListAdapter extends BaseAdapter {

		public MyListAdapter(Context context) {
			mContext = context;
		}

		@Override
		public int getCount() {
			return DAY.size();
		}

		@Override
		public boolean areAllItemsEnabled() {
			return true;
		}

		@Override
		public boolean isEnabled(int position) {
			return true;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			final int x = position;
			String headerText = HEADERS.get(position);
			if (!headerText.equals("EMPTY")) {

				View item = convertView;
				if (convertView == null || convertView.getTag() == LIST_ITEM) {

					item = LayoutInflater.from(mContext).inflate(
							R.layout.list_header_main_screen, parent, false);
					item.setTag(LIST_HEADER);

				}

				TextView headerTextView = (TextView) item
						.findViewById(R.id.list_header_main_screen_listHeader);
				headerTextView.setText(headerText);
				return item;
			}
			if (DAY.get(position) != null) {
				View item = convertView;
				if (convertView == null || convertView.getTag() == LIST_HEADER) {
					if(PLAYEDBEFORE.get(position)==0)
					{
						
						item = LayoutInflater.from(mContext).inflate(
								R.layout.list_row_games_not_attended, parent,
								false);
						
						item.setTag(LIST_ITEM);
						item.setOnClickListener(new OnClickListener() {
	
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Intent i = new Intent(context,
										GameProfileActivity.class);
								i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								i.putExtra("position", x);
								i.putExtra("icon", context.getFilesDir() + "/"
										+ ICONURLS.get(x).split("/")[3]
										+ ICONEXTENSION);
								context.startActivity(i);
							}
						});
						TextView day = (TextView) item
								.findViewById(R.id.list_row_games_not_attended_calendarDay);
						day.setText(DAY.get(position));
	
						TextView month = (TextView) item
								.findViewById(R.id.list_row_games_not_attended_calendarMonth);
						month.setText(MONTH.get(position));
						ImageView icon = (ImageView) item
								.findViewById(R.id.list_row_games_not_attended_gameIcon);
						TextView gameName = (TextView) item
								.findViewById(R.id.list_row_games_not_attended_gameName);
						TextView gameLocation = (TextView) item
								.findViewById(R.id.list_row_games_not_attended_gameLocation);
						gameName.setText(GAMENAMES.get(position));
						gameLocation.setText(GAMELOCATIONS.get(position));
						Bitmap iconBitmap = BitmapFactory.decodeFile(context
								.getFilesDir()
								+ "/"
								+ ICONURLS.get(position).split("/")[3]
								+ ICONEXTENSION);
						icon.setImageBitmap(iconBitmap);
						
						item.findViewById(R.id.list_row_games_not_attended_playButton).setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
							// TODO Auto-generated method stub
								final String documentId=DOCUMENTIDS.get(x);
								Intent intent=new Intent(context,LoadingActivity.class);
								intent.putExtra("status", 0);
								intent.putExtra("username", _username);
								intent.putExtra("gameDocumentId",documentId);
								
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								context.startActivity(intent);
								
							}
						});
						return item;
					}
					else
					{
						
						item = LayoutInflater.from(mContext).inflate(
								R.layout.list_row_previous_games, parent,
								false);
						
						item.setTag(LIST_ITEM);
						item.setOnClickListener(new OnClickListener() {
	
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Intent i = new Intent(context,
										GameProfileActivity.class);
								i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								i.putExtra("position", x);
								i.putExtra("icon", context.getFilesDir() + "/"
										+ ICONURLS.get(x).split("/")[3]
										+ ICONEXTENSION);
								context.startActivity(i);
							}
						});
						TextView day = (TextView) item
								.findViewById(R.id.list_row_previous_games_calendarDay);
						day.setText(DAY.get(position));
	
						TextView month = (TextView) item
								.findViewById(R.id.list_row_previous_games_calendarMonth);
						month.setText(MONTH.get(position));
						ImageView icon = (ImageView) item
								.findViewById(R.id.list_row_previous_games_gameIcon);
						TextView gameName = (TextView) item
								.findViewById(R.id.list_row_previous_games_gameName);
						TextView gameLocation = (TextView) item
								.findViewById(R.id.list_row_previous_games_gameLocation);
						gameName.setText(GAMENAMES.get(position));
						gameLocation.setText(GAMELOCATIONS.get(position));
						Bitmap iconBitmap = BitmapFactory.decodeFile(context
								.getFilesDir()
								+ "/"
								+ ICONURLS.get(position).split("/")[3]
								+ ICONEXTENSION);
						icon.setImageBitmap(iconBitmap);
						//ProgressBar progressBar=(ProgressBar) item.findViewById(R.id.list_row_previous_games_progressBar);
						//progressBar.setProgress(PASTPOINTS.get(position)/TOTALPOINTS.get(position));
						item.findViewById(R.id.list_row_previous_games_playButton).setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								final String documentId=DOCUMENTIDS.get(x);
								Intent intent=new Intent(context,LoadingActivity.class);
								intent.putExtra("status", 2);
								intent.putExtra("username", _username);
								intent.putExtra("gameDocumentId",documentId);
								
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								context.startActivity(intent);
							}
						});
						return item;
					}
				}
				
				
			}
			return null;

		}

		private final Context mContext;

	}

	public double getDistance(double lat1, double lon1) {
		NetworkLocation netLoc;
		netLoc = new NetworkLocation(this);
		double lat2 = netLoc.getLatitude();
		double lon2 = netLoc.getLongitude();
		double pk = (double) (180 / 3.14169);

		double a1 = lat2 / pk;
		double a2 = lon2 / pk;
		double b1 = lat1 / pk;
		double b2 = lon1 / pk;

		double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
		double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
		double t3 = Math.sin(a1) * Math.sin(b1);
		double c = Math.acos(t1 + t2 + t3);

		double R = 6371;
		double d = R * c;
		return d;

	}
	public void getGameInformation()
	{
		DatabaseHelper db=new DatabaseHelper(this);
		List<Game> gamesList=db.getAllGames();
		for(int i=0;i<gamesList.size();i++)
		{
			Game game=gamesList.get(i);
			LATITUDES.add(game.getGameLatitude());
			LONGITUDES.add(game.getGameLongitude());
			GAMENAMES.add(game.getGameName());
			GAMEDESCRIPTIONS.add(game.getGameDescription());
			GAMELOCATIONS.add(game.getLocationName());
			DOCUMENTIDS.add(game.getDocumentId());
			ICONURLS.add(game.getIconUrl());
			PLAYEDBEFORE.add(game.isPlayedBefore());
			SCORES.add(game.getScore());
			TOTALPOINTS.add(game.getTotalPath());
			PASTPOINTS.add(game.getPastPathCount());
			
			String lastDate=game.getLastDate();
			DateFormat jsonFormat = new SimpleDateFormat(
					"yyyy-MM-dd'T'HH:mm:ss", Locale
							.getDefault());
			DateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MMMM-dd'T'HH:mm:ss", Locale
							.getDefault());
			Date date;
			try {
				date = jsonFormat.parse(lastDate);
				String format = dateFormat.format(date);
				String month = format.split("-")[1];
				String day = lastDate.split("-")[2].split("T")[0];
				DAY.add(day);
				MONTH.add(month);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		isLoading = false;
		Fragment fragment = null;
		FragmentManager fragmentManager = getFragmentManager();
		fragment = new MainMenu();
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment).commit();
	}
	
	

	public boolean isConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean _isConnected = activeNetwork != null
				&& activeNetwork.isConnectedOrConnecting();
		return _isConnected;
	}

	public void initializeLists() {
		LATITUDES = new ArrayList<Double>();
		LONGITUDES = new ArrayList<Double>();
		DAY = new ArrayList<String>();
		MONTH = new ArrayList<String>();
		GAMENAMES = new ArrayList<String>();
		GAMELOCATIONS = new ArrayList<String>();
		GAMEDESCRIPTIONS = new ArrayList<String>();
		ICONURLS = new ArrayList<String>();
		HEADERS = new ArrayList<String>(Collections.nCopies(1000, "EMPTY"));
		DOCUMENTIDS = new ArrayList<String>();
		PLAYEDBEFORE = new ArrayList<Integer>();
		SCORES = new ArrayList<Integer>();
		TOTALPOINTS = new ArrayList<Integer>();
		PASTPOINTS = new ArrayList<Integer>();
	}

	public static void setHeader(int position, String headerCurrentGames) {
		HEADERS.set(position, headerCurrentGames);
		DAY.add(null);
		MONTH.add(null);
		GAMENAMES.add(null);
		GAMELOCATIONS.add(null);
		GAMEDESCRIPTIONS.add(null);
		ICONURLS.add(null);
		LATITUDES.add(null);
		LONGITUDES.add(null);
		DOCUMENTIDS.add(null);
		PLAYEDBEFORE.add(null);
		SCORES.add(null);
		TOTALPOINTS.add(null);
		PASTPOINTS.add(null);
	}
	public static void clearAllLists()
	{
		//HEADERS.clear();
		DAY.clear();
		MONTH.clear();
		GAMENAMES.clear();
		GAMELOCATIONS.clear();
		GAMEDESCRIPTIONS.clear();
		ICONURLS.clear();
		LATITUDES.clear();
		LONGITUDES.clear();
		DOCUMENTIDS.clear();
		PLAYEDBEFORE.clear();
		SCORES.clear();
		TOTALPOINTS.clear();
		PASTPOINTS.clear();
		DAY.add(null);
		MONTH.add(null);
		GAMENAMES.add(null);
		GAMELOCATIONS.add(null);
		GAMEDESCRIPTIONS.add(null);
		ICONURLS.add(null);
		LATITUDES.add(null);
		LONGITUDES.add(null);
		DOCUMENTIDS.add(null);
		PLAYEDBEFORE.add(null);
		SCORES.add(null);
		TOTALPOINTS.add(null);
		PASTPOINTS.add(null);
	}
	public static void refreshTheDatabase()
	{
		final DatabaseHelper db=new DatabaseHelper(context);
		clearAllLists();
		//setHeader(DAY.size(), context.getString(R.string.header_current_games));
		final StorageService storageService = App42API.buildStorageService();
		storageService.findAllDocuments(DBNAME, COLLECTIONNAME,
				new App42CallBack() {

					@Override
					public void onSuccess(Object response) {
						// TODO Auto-generated method stub
						// disableLoadingLayer();
						Storage storage = (Storage) response;
						ArrayList<Storage.JSONDocument> jsonDocList = storage
								.getJsonDocList();

						for (int i = 0; i < jsonDocList.size(); i++) {

							try {
								final Game game=new Game();
								JSONObject json = new JSONObject(jsonDocList
										.get(i).getJsonDoc());
								double lat = json.getDouble("GameLatitude");
								double lon = json.getDouble("GameLongitude");
								// if (getDistance(lat, lon) < 100) {
								game.setGameLatitude(lat);
								game.setGameLongitude(lon);
								game.setDocumentId(jsonDocList.get(i).getDocId());
								game.setLastDate(json.getString("LastDate"));
								String iconUrl = json.getString("ImagePath");
								game.setIconUrl(iconUrl);
								game.setGameName(json.getString("GameName"));
								if (!context.getFileStreamPath(
										iconUrl.split("/")[3] + ICONEXTENSION)
										.exists()) {

									
									FileDownloader downloadIcon = new FileDownloader();
									downloadIcon.context = context;
									downloadIcon.setFile(iconUrl.split("/")[3]
											+ ICONEXTENSION);
									downloadIcon.execute(iconUrl);
								}

								game.setLocationName(json.getString("LocationName"));
								game.setGameDescription(json.getString("GameDescription")) ;
								Query q1=QueryBuilder.build("username", _username,QueryBuilder.Operator.EQUALS);
								Query q2=QueryBuilder.build("gameDocumentId", jsonDocList.get(i).getDocId(), Operator.EQUALS);
								Query query=QueryBuilder.compoundOperator(q1, Operator.AND, q2);
								storageService.findDocumentsByQuery(DBNAME, "UsersGamesCollection", query ,new App42CallBack() {
									
									@Override
									public void onSuccess(Object arg0) {
										// TODO Auto-generated method stub
										Storage str=(Storage)arg0;
										try {
											JSONObject historyJson= new JSONObject(str.getJsonDocList().get(0).getJsonDoc());
											game.setPlayedBefore(1);
											game.setScore(historyJson.getInt("score"));
											if(!historyJson.getString("currentPoint").equals(""))
											{
												JSONArray rList=historyJson.getJSONArray("remainingPoints");
												JSONArray pList=historyJson.getJSONArray("pastPoints");
												int total=1+rList.length()+pList.length();
												game.setTotalPath(total);
												game.setPastPathCount(pList.length());
												db.updateGame(game);
											}
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											game.setPlayedBefore(0);
											
											e.printStackTrace();
										}
									}
									
									@Override
									public void onException(Exception arg0) {
										// TODO Auto-generated method stub
										
										db.updateGame(game);
									}
								});
								
								
							} catch (JSONException e) {
								// disableLoadingLayer();
								// TODO Auto-generated catch block
								e.printStackTrace();
							} 
						}

					}

					@Override
					public void onException(Exception response) {
						// TODO Auto-generated method stub
						// disableLoadingLayer();
					}
				});
		
	}
	public static void startQuestionActivity(int questionType,String questionInfo,String imageUrl,String gameDocumentId)
	{
		switch(questionType){
			case 0:
			{
				Intent i=new Intent(context,OnlyQuestion_Activity.class);
				i.putExtra("questionInfo", questionInfo);
				i.putExtra("gameDocumentId", gameDocumentId);
				i.putExtra("username", _username);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(i);
				
				break;
			}
			case 1:
			{
				break;
			}
			case 2:
			{
				break;
			}
			default:
				break;
		}
	}
	
	
	

}