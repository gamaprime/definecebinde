package com.gamaprime.definecebinde;

import org.json.JSONException;
import org.json.JSONObject;

import com.shephertz.app42.paas.sdk.android.App42API;
import com.shephertz.app42.paas.sdk.android.App42CallBack;
import com.shephertz.app42.paas.sdk.android.customcode.CustomCodeService;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class Mission_Text_Only extends Activity {
	public static String TAG;
	private NfcAdapter mNfcAdapter;
	public static final String MIME_TEXT_PLAIN = "text/plain";
	private int MISSON_TYPE;
	private String username;
	private String gameDocumentId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mission_text_only);
		String questionInfo=getIntent().getStringExtra("questionInfo");
		MISSON_TYPE=getIntent().getIntExtra("missionType", -1);
		username=getIntent().getStringExtra("username");
		gameDocumentId=getIntent().getStringExtra("gameDocumentId");
		TextView questionField= (TextView) findViewById(R.id.mission_text_questionField);
		questionField.setText(questionInfo);
		findViewById(R.id.mission_text_checkButton).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(MISSON_TYPE==0)
				{
					TAG="onur";
					checkTheResult();
					//startQR();
				}
			}
		});
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {

			if (resultCode == RESULT_OK) {
				TAG=intent.getStringExtra("SCAN_RESULT");
				checkTheResult();
			} 
			else if (resultCode == RESULT_CANCELED) {
				
			}
		}
	}
	private void handleIntent(Intent intent) {
   	 NFCReader reader = new NFCReader(this);
   	
   	 String action = intent.getAction();
   	    if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
   	        String type = intent.getType();
   	        if (MIME_TEXT_PLAIN.equals(type)) {
   	            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
   	            reader.ReadTag(tag);
   	        } else {
   	            //Log.d(TAG, "Wrong mime type: " + type);
   	        }
   	    } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {
   	        // In case we would still use the Tech Discovered Intent
   	        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
   	        String[] techList = tag.getTechList();
   	        String searchedTech = Ndef.class.getName();
   	        for (String tech : techList) {
   	            if (searchedTech.equals(tech)) {
   	            	reader.ReadTag(tag);
   	                break;
   	            }
   	        }
   	    }
	}
	public static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);
        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};
        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType(MIME_TEXT_PLAIN);
        } catch (MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }
        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }
	public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }
	@Override
    protected void onResume() {
        super.onResume();
        if(MISSON_TYPE==1)
        	setupForegroundDispatch(this, mNfcAdapter);
    }
    @Override
    protected void onPause() {
    	if(MISSON_TYPE==1)
    		stopForegroundDispatch(this, mNfcAdapter);
        super.onPause();
    }
    @Override
    protected void onNewIntent(Intent intent) {
        
    	if(MISSON_TYPE==1)
    		handleIntent(intent);
    }
    public void startQR()
    {
    	Intent intent = new Intent(
				"com.google.zxing.client.android.SCAN");
		intent.setPackage(getPackageName());
		intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
		startActivityForResult(intent, 0);
    }
    public void checkTheResult()
    {
    	CustomCodeService customCode=App42API.buildCustomCodeService();
    	JSONObject json=new JSONObject();
    	try {
			json.put("username", username);
			json.put("gameDocumentId", gameDocumentId);
			Log.d(username,gameDocumentId);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	if(MISSON_TYPE==0 || MISSON_TYPE==1)
    	{
    		try {
				json.put("tag", TAG);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		customCode.runJavaCode("MissionSystem", json,new App42CallBack() {
				
				@Override
				public void onSuccess(Object arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onException(Exception arg0) {
					// TODO Auto-generated method stub
					
				}
			});
    	}
    }
}
