package com.gamaprime.definecebinde;

public class Game {
	private String documentId;
	private String locationName;
	private String gameName;
	private String zipPath;
	private String gameDescription;
	private double gameLongitude;
	private double gameLatitude;
	private int playedBefore;
	private String lastDate;
	private String iconUrl;
	private int totalPath;
	private int pastPathCount;
	private int score;
	public Game(){}
	
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getZipPath() {
		return zipPath;
	}

	public void setZipPath(String zipPath) {
		this.zipPath = zipPath;
	}

	public String getGameDescription() {
		return gameDescription;
	}

	public void setGameDescription(String gameDescription) {
		this.gameDescription = gameDescription;
	}

	public double getGameLongitude() {
		return gameLongitude;
	}

	public void setGameLongitude(double gameLongitude) {
		this.gameLongitude = gameLongitude;
	}

	public double getGameLatitude() {
		return gameLatitude;
	}

	public void setGameLatitude(double gameLatitude) {
		this.gameLatitude = gameLatitude;
	}

	public int isPlayedBefore() {
		return playedBefore;
	}

	public void setPlayedBefore(int playedBefore) {
		this.playedBefore = playedBefore;
	}

	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	

	public int getPastPathCount() {
		return pastPathCount;
	}

	public void setPastPathCount(int pastPathCount) {
		this.pastPathCount = pastPathCount;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getTotalPath() {
		return totalPath;
	}

	public void setTotalPath(int totalPath) {
		this.totalPath = totalPath;
	}
}
