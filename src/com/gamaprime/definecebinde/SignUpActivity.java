package com.gamaprime.definecebinde;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.view.View.OnClickListener;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.google.android.gms.common.*;
import com.google.android.gms.common.GooglePlayServicesClient.*;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.model.people.Person;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.shephertz.app42.paas.sdk.android.App42API;
import com.shephertz.app42.paas.sdk.android.App42CallBack;
import com.shephertz.app42.paas.sdk.android.App42Exception;
import com.shephertz.app42.paas.sdk.android.avatar.Avatar;
import com.shephertz.app42.paas.sdk.android.avatar.AvatarService;
import com.shephertz.app42.paas.sdk.android.storage.Query;
import com.shephertz.app42.paas.sdk.android.storage.QueryBuilder;
import com.shephertz.app42.paas.sdk.android.storage.Storage;
import com.shephertz.app42.paas.sdk.android.storage.StorageService;
import com.shephertz.app42.paas.sdk.android.storage.QueryBuilder.Operator;
import com.shephertz.app42.paas.sdk.android.user.User;
import com.shephertz.app42.paas.sdk.android.user.UserService;
import com.shephertz.app42.paas.sdk.android.user.User.Profile;

public class SignUpActivity extends Activity implements ConnectionCallbacks,
		OnConnectionFailedListener {
	private static final int REQUEST_CODE_RESOLVE_ERR = 9000;
	public String username;
	public String emailAddress;
	public String password;
	private ProgressDialog mConnectionProgressDialog;
	private PlusClient mPlusClient;
	private ConnectionResult mConnectionResult;
	private RelativeLayout loadingLayer;
	private static String DBNAME = "GAMESDB";
	private static String COLLECTIONNAME = "GamesCollection";
	private static String ICONEXTENSION = "_icon.jpg";
	private DatabaseHelper db;
	private int counter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		db=new DatabaseHelper(this);
        db.dropTable();
        db.createTable();
		setContentView(R.layout.activity_signup);
		mPlusClient = new PlusClient.Builder(this, this, this)
				.setActions("http://schemas.google.com/AddActivity",
						"http://schemas.google.com/BuyActivity")
				.setScopes(Scopes.PLUS_LOGIN) // recommended login scope for
												// social features
				// .setScopes("profile") // alternative basic login scope
				.build();
		// Progress bar to be displayed if the connection failure is not
		// resolved.
		mConnectionProgressDialog = new ProgressDialog(this);
		mConnectionProgressDialog.setMessage("Signing in...");
		loadingLayer=(RelativeLayout)findViewById(R.id.activity_signup_loadingFrame);
       	loadingLayer.setVisibility(View.GONE);
		findViewById(R.id.activity_signup_googleConnectButton)
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						plusSignUp(v);
					}
				});
		Button facebookLoginButton = (Button) findViewById(R.id.activity_signup_facebookConnectButton);
		facebookLoginButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				facebookSignUp();
			}
		});
		Button emailSignUpButton = (Button) findViewById(R.id.activity_signup_signUpButton);
		emailSignUpButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				emailSignUp();
			}
		});

	}
	public void enableLoadingLayer() {
		loadingLayer.setVisibility(View.VISIBLE);
	}

	public void disableLoadingLayer() {
		loadingLayer.setVisibility(View.GONE);
	}
	public void showErrorMessage(final int message)
	{
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
				builder.setTitle(R.string.error);
				builder.setMessage(message);
				builder.setNegativeButton(R.string.ok_button, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
				AlertDialog alertDialog=builder.create();
				alertDialog.show();
				disableLoadingLayer();
			}
		});
	}
	public void emailSignUp() {
		EditText usernameText = (EditText) findViewById(R.id.activity_signup_usernameTextInput);
		EditText mailText = (EditText) findViewById(R.id.activity_signup_emailTextInput);
		EditText passwordText = (EditText) findViewById(R.id.activity_signup_passwordTextInput);
		emailAddress = mailText.getText().toString();
		password = passwordText.getText().toString();
		username = usernameText.getText().toString();
		if(!emailAddress.equals("") && !password.equals("") && !username.equals(""))
		{
			enableLoadingLayer();
			
			Editor edit = PreferenceManager.getDefaultSharedPreferences(
					getBaseContext()).edit();
			edit.putBoolean("IsLoggedIn", true);
			edit.commit();
			UserService userService = App42API.buildUserService();
			userService.createUser(username, password, emailAddress,
					new App42CallBack() {
						public void onSuccess(Object response) {
							createGamesDatabase(username);
							
						}

						public void onException(Exception ex) {
							App42Exception exception = (App42Exception) ex;
							int appErrorCode = exception.getAppErrorCode();
							if (appErrorCode == 2001) {
								disableLoadingLayer();
								showErrorMessage(R.string.username_exists_error);
							} else if (appErrorCode == 2005) {
								disableLoadingLayer();
								showErrorMessage(R.string.email_exists_error);
								// Handle here for Bad Request (The request
								// parameters are invalid. User with emailId
								// 'nick@shephertz.co.in' already exists.)
							} else if (appErrorCode == 1500) {
								disableLoadingLayer();
								showErrorMessage(R.string.internal_server_error);
							}
							else if(appErrorCode==1400)
							{
								disableLoadingLayer();
								showErrorMessage(R.string.invalid_error);
								
							}
						}
					});
		}
		else
		{
			disableLoadingLayer();
			showErrorMessage(R.string.empty_error);
		}
		
	}

	public void facebookSignUp() {
		enableLoadingLayer();
		List<String> permissions = Arrays.asList("basic_info",ParseFacebookUtils.Permissions.User.EMAIL);
		ParseFacebookUtils.logIn(permissions, this, new LogInCallback() {
			@Override
			public void done(ParseUser user, ParseException err) {
				if(user==null)
					disableLoadingLayer();
				else
				{
					Session session = ParseFacebookUtils.getSession();
				    if (session != null && session.isOpened()) {
				    	
				        makeMeRequest();
				    }
				}
			}
		});
	}

	public void plusSignUp(View v) {
		if (v.getId() == R.id.activity_signup_googleConnectButton
				&& !mPlusClient.isConnected()) {
			if (mConnectionResult == null) {
				mConnectionProgressDialog.show();
			} else {
				try {

					mConnectionResult.startResolutionForResult(this,
							REQUEST_CODE_RESOLVE_ERR);
				} catch (SendIntentException e) {
					// Try connecting again.
					mConnectionResult = null;
					mPlusClient.connect();
				}
			}
		}
	}
	
	protected void makeMeRequest() {
		Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),new Request.GraphUserCallback() {
			@Override
			public void onCompleted(final GraphUser user, Response response) {
				if (user != null) {
					
					final Editor edit = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
					
					final UserService userService = App42API.buildUserService();
					
					userService.createUser(user.getId(), "password",(String) user.getProperty("email"),new App42CallBack() {
						public void onSuccess(Object response) {
							User appUser=(User)response;
							Profile profile = appUser.new Profile();
							profile.setFirstName((String) user.getProperty("first_name"));
							profile.setLastName((String) user.getProperty("last_name"));
							edit.putBoolean("IsLoggedIn", true);
							edit.putString("username", user.getId());
							edit.commit();
							AvatarService avatarService = App42API.buildAvatarService();
							Session session=ParseFacebookUtils.getSession();
							avatarService.createAvatarFromFacebook("Avatar"+user.getId() ,user.getId(), session.getAccessToken(),
									"Avatar of " + user.getId(),
									new App42CallBack() {

										@Override
										public void onSuccess(Object arg0) {
											// TODO Auto-generated method stub
											
										}

										@Override
										public void onException(Exception arg0) {
											// TODO Auto-generated method stub

										}
									});
							avatarService.getCurrentAvatar(user.getId(), new App42CallBack() {
								
								@Override
								public void onSuccess(Object arg0) {
									// TODO Auto-generated method stub
									Avatar result=(Avatar)arg0;
									String url=result.getURL();
									downloadImage(Environment.getExternalStorageDirectory()+"", Environment.DIRECTORY_PICTURES+"/Hello", user.getId()+".jpg", url);
								}
								
								@Override
								public void onException(Exception arg0) {
									// TODO Auto-generated method stub
									
								}
							});
							createGamesDatabase(user.getId());
							
						}
						public void onException(Exception ex) {
							App42Exception exception = (App42Exception) ex;
							int appErrorCode = exception.getAppErrorCode();
							
							if (appErrorCode == 2001) {
								edit.putBoolean("IsLoggedIn", true);
								edit.putString("username", user.getId());
								edit.commit();
								AvatarService avatarService = App42API.buildAvatarService();
								avatarService.getCurrentAvatar(user.getId(), new App42CallBack() {
									
									@Override
									public void onSuccess(Object arg0) {
										// TODO Auto-generated method stub
										Avatar result=(Avatar)arg0;
										String url=result.getURL();
										downloadImage(Environment.getExternalStorageDirectory()+"", Environment.DIRECTORY_PICTURES+"/Hello", user.getId()+".jpg", url);
									}
									
									@Override
									public void onException(Exception arg0) {
										// TODO Auto-generated method stub
										
									}
								});
								createGamesDatabase(user.getId());
							} else if (appErrorCode == 2005) {
								showErrorMessage(R.string.email_exists_error);
							} else if (appErrorCode == 1500) {
								showErrorMessage(R.string.internal_server_error);
							}
						}
					});
					
					
				}

			}
		});
		request.executeAsync();

	}


	@Override
	protected void onStart() {
		super.onStart();
		mPlusClient.connect();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if(mPlusClient.isConnected())
        {
        	mPlusClient.clearDefaultAccount();
            mPlusClient.disconnect();
        }
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (mConnectionProgressDialog.isShowing()) {
			// The user clicked the sign-in button already. Start to resolve
			// connection errors. Wait until onConnected() to dismiss the
			// connection dialog.
			if (result.hasResolution()) {
				try {
					result.startResolutionForResult(this,
							REQUEST_CODE_RESOLVE_ERR);
				} catch (SendIntentException e) {
					mPlusClient.connect();
				}
			}
		}
		// Save the result and resolve the connection failure upon a user click.
		mConnectionResult = result;
	}

	@Override
	protected void onActivityResult(int requestCode, int responseCode,
			Intent intent) {
		super.onActivityResult(requestCode, responseCode, intent);
		if (requestCode == REQUEST_CODE_RESOLVE_ERR
				&& responseCode == RESULT_OK) {
			mConnectionResult = null;
			mPlusClient.connect();
		} else
			ParseFacebookUtils.finishAuthentication(requestCode, responseCode,intent);
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		
		
		final Person currentPerson = mPlusClient.getCurrentPerson();
		final String webUrl;
		if(currentPerson.hasImage())
			webUrl=currentPerson.getImage().getUrl();
		else
			webUrl="";
		final String username = currentPerson.getId();
		
		
		
		emailAddress = mPlusClient.getAccountName();
		final UserService userService = App42API.buildUserService();
		userService.createUser(username, "password", emailAddress,
				new App42CallBack() {
					public void onSuccess(Object response) {
						User appUser=(User)response;
						Profile profile = appUser.new Profile();
						Date date=new Date();
						profile.setFirstName(currentPerson.getDisplayName());  
						DefineCebindeApplication.fullName=currentPerson.getDisplayName();
						userService.createOrUpdateProfile(appUser);
						AvatarService avatarService=App42API.buildAvatarService();
						avatarService.createAvatarFromWebURL("AvatarPlus"+username, username, webUrl, "Avatar of "+username, new App42CallBack() {
							
							@Override
							public void onSuccess(Object arg0) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void onException(Exception arg0) {
								// TODO Auto-generated method stub
								
							}
						});
						downloadImage(Environment.getExternalStorageDirectory()+"", Environment.DIRECTORY_PICTURES+"/Hello", username+".jpg", webUrl);
						Editor edit = PreferenceManager.getDefaultSharedPreferences(
								getBaseContext()).edit();
						edit.putBoolean("IsLoggedIn", true);
						edit.putString("username", username);
						edit.commit();
						createGamesDatabase(username);
						
					}
					public void onException(Exception ex) {
						App42Exception exception = (App42Exception) ex;
						int appErrorCode = exception.getAppErrorCode();
						if (appErrorCode == 2001) {
							downloadImage(Environment.getExternalStorageDirectory()+"", Environment.DIRECTORY_PICTURES+"/Hello", username+".jpg", webUrl);
							Editor edit = PreferenceManager.getDefaultSharedPreferences(
									getBaseContext()).edit();
							edit.putBoolean("IsLoggedIn", true);
							edit.putString("username", username);
							edit.commit();
							AvatarService avatarService = App42API.buildAvatarService();
							avatarService.getCurrentAvatar(username, new App42CallBack() {
								
								@Override
								public void onSuccess(Object arg0) {
									// TODO Auto-generated method stub
									Avatar result=(Avatar)arg0;
									String url=result.getURL();
									downloadImage(Environment.getExternalStorageDirectory()+"", Environment.DIRECTORY_PICTURES+"/Hello", username+".jpg", url);
								}
								
								@Override
								public void onException(Exception arg0) {
									// TODO Auto-generated method stub
									
								}
							});
							//Intent intent=new Intent(SignInActivity.this,MenuActivity.class);
							
							createGamesDatabase(username);
						} else if (appErrorCode == 2005) {
							showErrorMessage(R.string.email_exists_error);
						} else if (appErrorCode == 1500) {
							showErrorMessage(R.string.internal_server_error);
						}
					}
				});
	}


	@Override
	public void onDisconnected() {
		
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		/*Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();*/
	}
	public void downloadImage(String path,String directory,String file,String url)
    {
    	FileDownloader imageDownloader=new FileDownloader();
    	imageDownloader.setPath(path);
    	imageDownloader.setDirectory(directory);
    	imageDownloader.setFile(file);
    	imageDownloader.execute(url);
    	
    }
	public void createGamesDatabase(final String username)
	{
		counter = 0;
		final StorageService storageService = App42API.buildStorageService();
		storageService.findAllDocuments(DBNAME, COLLECTIONNAME,
				new App42CallBack() {

					@Override
					public void onSuccess(Object response) {
						// TODO Auto-generated method stub
						// disableLoadingLayer();
						Storage storage = (Storage) response;
						ArrayList<Storage.JSONDocument> jsonDocList = storage
								.getJsonDocList();
						final int size=jsonDocList.size();
						
						for (int i = 0; i < size; i++) {

							try {
								final Game game=new Game();
								JSONObject json = new JSONObject(jsonDocList
										.get(i).getJsonDoc());
								double lat = json.getDouble("GameLatitude");
								double lon = json.getDouble("GameLongitude");
								// if (getDistance(lat, lon) < 100) {
								game.setGameLatitude(lat);
								game.setGameLongitude(lon);
								game.setDocumentId(jsonDocList.get(i).getDocId());
								game.setLastDate(json.getString("LastDate"));
								String iconUrl = json.getString("ImagePath");
								game.setIconUrl(iconUrl);
								game.setGameName(json.getString("GameName"));
								if (!getFileStreamPath(
										iconUrl.split("/")[3] + ICONEXTENSION)
										.exists()) {

									
									FileDownloader downloadIcon = new FileDownloader();
									downloadIcon.context = getApplicationContext();
									downloadIcon.setFile(iconUrl.split("/")[3]
											+ ICONEXTENSION);
									downloadIcon.execute(iconUrl);
								}

								game.setLocationName(json.getString("LocationName"));
								game.setGameDescription(json.getString("GameDescription")) ;
								Query q1=QueryBuilder.build("username", username,QueryBuilder.Operator.EQUALS);
								Query q2=QueryBuilder.build("gameDocumentId", jsonDocList.get(i).getDocId(), Operator.EQUALS);
								Query query=QueryBuilder.compoundOperator(q1, Operator.AND, q2);
								storageService.findDocumentsByQuery(DBNAME, "UsersGamesCollection", query, new App42CallBack() {
									
									

									@Override
									public void onSuccess(Object arg0) {
										// TODO Auto-generated method stub
										counter++;
										Storage str=(Storage)arg0;
										try {
											JSONObject historyJson= new JSONObject(str.getJsonDocList().get(0).getJsonDoc());
											game.setPlayedBefore(1);
											game.setScore(historyJson.getInt("score"));
											if(!historyJson.getString("currentPoint").equals(""))
											{
												JSONArray rList=historyJson.getJSONArray("remainingPoints");
												JSONArray pList=historyJson.getJSONArray("pastPoints");
												int total=1+rList.length()+pList.length();
												game.setTotalPath(total);
												game.setPastPathCount(pList.length());
												db.addGame(game);
											}
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										if(counter==size)
										{
											Intent i=new Intent(SignUpActivity.this,Activity_Navigation.class);
											startActivity(i);
											finish();
										}
									}
									
									@Override
									public void onException(Exception arg0) {
										// TODO Auto-generated method stub
										
										db.addGame(game);
										counter++;
										if(counter==size)
										{
											Intent i=new Intent(SignUpActivity.this,Activity_Navigation.class);
											startActivity(i);
											finish();
										}
									}
								});
								
								
							} catch (JSONException e) {
								// disableLoadingLayer();
								// TODO Auto-generated catch block
								e.printStackTrace();
							} 
						}
						
						
						
					}

					@Override
					public void onException(Exception response) {
						// TODO Auto-generated method stub
						// disableLoadingLayer();
					}
				});
		
		
	}
}
