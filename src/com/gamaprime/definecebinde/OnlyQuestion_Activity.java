package com.gamaprime.definecebinde;

import org.json.JSONException;
import org.json.JSONObject;

import com.shephertz.app42.paas.sdk.android.App42API;
import com.shephertz.app42.paas.sdk.android.App42CallBack;
import com.shephertz.app42.paas.sdk.android.customcode.CustomCodeService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class OnlyQuestion_Activity extends Activity {
	private static final String ANSWER_SYSTEM="AnswerSystem";
	private static final String CORRECT_ANSWER="True";
	private static final String WRONG_ANSWER="False";
	private String username;
	private String gameDocumentId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mission_text_input_01);
		String questionInfo=getIntent().getStringExtra("questionInfo");
		username=getIntent().getStringExtra("username");
		gameDocumentId=getIntent().getStringExtra("gameDocumentId");
		
		TextView questionField= (TextView) findViewById(R.id.mission_text_input_01_questionField);
		questionField.setText(questionInfo);
		final EditText answerField=(EditText)findViewById(R.id.mission_text_input_01_answerField);
		findViewById(R.id.mission_text_input_01_submit_button).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CustomCodeService submitAnswer=App42API.buildCustomCodeService();
				JSONObject json=new JSONObject();
				try {
					json.put("username", username);
					json.put("gameDocumentId", gameDocumentId);
					json.put("answer", answerField.getText().toString());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				submitAnswer.runJavaCode(ANSWER_SYSTEM, json, new App42CallBack() {
					
					@Override
					public void onSuccess(Object response) {
						// TODO Auto-generated method stub
						JSONObject responseJson=(JSONObject)response;
						String result="";
						try {
							result=responseJson.getString("result");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(result.equals(CORRECT_ANSWER))
						{
							int questionType=-1;
							int missionType=-1;
							String questionInfo="";
							String imageUrl="";
							try {
								missionType=responseJson.getInt("missionType");
								questionType=responseJson.getInt("questionType");
								questionInfo=responseJson.getString("questionText");
								imageUrl=responseJson.getString("imageUrl");
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							Intent intent=new Intent(OnlyQuestion_Activity.this,LoadingActivity.class);
							intent.putExtra("username", username);
							intent.putExtra("gameDocumentId", gameDocumentId);
							intent.putExtra("missionType", missionType);
							intent.putExtra("questionType", questionType);
							intent.putExtra("questionInfo", questionInfo);
							intent.putExtra("imageUrl", imageUrl);
							intent.putExtra("status", 1);
							startActivity(intent);
							finish();
							
						}
						else
						{
							runOnUiThread(new Runnable() {
								public void run() {
									Toast.makeText(getApplicationContext(),"WRONG ANSWER", Toast.LENGTH_LONG).show();
								}
							});
						}
					}
					
					@Override
					public void onException(Exception arg0) {
						// TODO Auto-generated method stub
						
					}
				});
			}
		});
	}
	
	public void startMissionActivity(int questionType,String questionInfo,String imageUrl,String gameDocumentId)
	{
		switch(questionType){
			case 0:
			{
				Intent i=new Intent(OnlyQuestion_Activity.this, OnlyQuestion_Activity.class);
				i.putExtra("questionInfo", questionInfo);
				i.putExtra("gameDocumentId", gameDocumentId);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
				
				break;
			}
			case 1:
			{
				break;
			}
			case 2:
			{
				break;
			}
			default:
				break;
		}
	}
}
