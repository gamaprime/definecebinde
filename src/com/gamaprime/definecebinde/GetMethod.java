package com.gamaprime.definecebinde;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

public class GetMethod extends HttpMethods {

	@Override
	public String makeRequest(String url, JSONObject jsonObject) {
		// TODO Auto-generated method stub
		InputStream content = null;
		String result = null;
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(new HttpGet(url));
			content = response.getEntity().getContent();
			if (content != null)
				result = convertInputStreamToString(content);
			else
				result = "Did not work!";

		} catch (Exception e) {
			return null;
		}
		return result;

	}


}
