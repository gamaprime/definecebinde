package com.gamaprime.definecebinde;

import java.util.ArrayList;

import com.shephertz.app42.paas.sdk.android.App42API;
import com.shephertz.app42.paas.sdk.android.App42CallBack;
import com.shephertz.app42.paas.sdk.android.game.Game.Score;
import com.shephertz.app42.paas.sdk.android.game.ScoreBoardService;

import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class GameProfileActivity extends Activity {
	public static int position;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_profile);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		TextView title=(TextView)findViewById(R.id.activity_game_profile_gameTitleText);
		TextView info=(TextView)findViewById(R.id.activity_game_profile_gameInfoText);
		TextView description=(TextView)findViewById(R.id.activity_game_profile_gameDescriptionText);
		ImageView icon=(ImageView)findViewById(R.id.activity_game_profile_gameIconImage);
		position=getIntent().getIntExtra("position", -1);
		if(position!=-1)
		{
			title.setText(Activity_Navigation.GAMENAMES.get(position));
			description.setText(Activity_Navigation.GAMEDESCRIPTIONS.get(position));
			info.setText(Activity_Navigation.GAMELOCATIONS.get(position));
			Bitmap iconBitmap=BitmapFactory.decodeFile(getIntent().getStringExtra("icon"));
			icon.setImageBitmap(iconBitmap);
		}
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.shownmap, menu);
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		// Handle action buttons
		switch (item.getItemId()) {
		case R.id.showOnMap:
			// create intent to perform web search for this planet
			//Google Maps intent
			 
			 Intent intent = new Intent(this, MapActivity.class);
			 intent.putExtra("position", position);
			 
			 startActivity(intent);
			 
			 
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
