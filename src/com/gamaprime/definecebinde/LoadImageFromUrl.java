package com.gamaprime.definecebinde;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

public class LoadImageFromUrl extends AsyncTask<String, Void, Bitmap> {
	public static ImageView imageView;
	@Override
	protected Bitmap doInBackground(String... params) {
		// TODO Auto-generated method stub
		try{
        	URL url=new URL(params[0]);
        	HttpURLConnection con=(HttpURLConnection)url.openConnection();
        	con.setDoInput(true);
        	con.connect();
        	InputStream inp=con.getInputStream();
        	return BitmapFactory.decodeStream(inp);
        	
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	return null;
        }
		
	}
	protected void onPostExecute(Bitmap result) {
		imageView.setImageBitmap(result);
	}
		
}

